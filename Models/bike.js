var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bikeSchema = new Schema({
    bike_id: Number,
    color: String,
    bike_model: String,
    location: {
        type: [Number], index: { type: '2dsphere', sparse: true }
    } 

});

bikeSchema.statics.createInstance = function(bike_id,color,bike_model,location){
    return new this({
        bike_id: bike_id,
        color: color,
        bike_model: bike_model,
        location: location
    });
};


bikeSchema.method.toString = function() {
    return 'id: '+ this.bike_id + " | color: " + this.color;
};

bikeSchema.statics.allBikes = function(cb){
    return this.find({},cb);
};

bikeSchema.statics.add = function(aBike,cb){
    this.create(aBike,cb);
};

bikeSchema.statics.findById = function(bike_id, cb){
    return this.findOne({bike_id: bike_id}, cb);
};

bikeSchema.statics.removeById = function(bike_id, cb){
    return this.deleteOne({bike_id: bike_id}, cb);
}

bikeSchema.statics.updateBike = function(bike_id, bike, cb){
    return this.updateOne({bike_id: bike_id},bike,cb);
}

module.exports = mongoose.model('Bike', bikeSchema);