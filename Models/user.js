var mongoose = require('mongoose');
var Reservation = require('./reservation');
var Schema = mongoose.Schema;


var userSchema = new Schema({
    name: String
});

userSchema.methods.reserve= function(bikeId,from,to,cb){
    var reservation = new Reservation({ user: this.id, bike: bikeId, from: from, to: to});
    //console.log(reservation);
    reservation.save(cb);
}

module.exports = mongoose.model('User', userSchema);