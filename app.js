var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bikesRouter = require('./routes/bikes');
var bikeAPIRouter = require('./routes/api/bikes');
var userAPIRouter = require('./routes/api/users');
var reservationAPIRouter = require('./routes/api/reservations');

var app = express();

var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/bikes_network';

mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on('error',console.error.bind(console,"MongoDB connection error: "));
db.once('open', function(){
  console.log('we are connected to test database');
            
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bikes',bikesRouter);
app.use('/api/bikes',bikeAPIRouter);
app.use('/api/users',userAPIRouter);
app.use('/api/reservations',reservationAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
