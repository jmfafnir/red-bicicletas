var Bike = require('../../Models/bike');

exports.bike_list = function (req, res) {
    Bike.allBikes((err,bikes)=>{
        res.status(200).json({
            bikes: bikes
        })
    })
    
}

exports.bike_create = function(req, res){
    obj = {
        bike_id: req.body.bike_id,
        color: req.body.color, 
        bike_model:req.body.bike_model,
        location: [req.body.lat, req.body.lng]
    }    
    var bike = new Bike(obj);
    Bike.add(bike);

    res.status(200).json({
        bike: bike
    });
}

exports.bike_delete = function (req, res) {
    Bike.removeById(req.body.bike_id,()=>{
        res.status(200).json({
            
        });
    });

    
}

exports.bike_update = function(req, res){
    obj = {
        bike_id: req.body.bike_id,
        color: req.body.color, 
        bike_model:req.body.bike_model,
        location: [req.body.lat, req.body.lng]
    };   
    Bike.updateBike(req.query.bike_id,obj,(err,bike_update_status)=>{ 
        res.status(200).json({
            status: bike_update_status
        });
    });

    
}