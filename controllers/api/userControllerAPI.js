var User = require('../../Models/user');

exports.user_list = function(req,res){
    User.find({}, (err,users)=>{
        res.status(200).json({
            users: users
        });
    })
}

exports.user_create = function(req,res){
    var user = new User({name: req.body.name});

    user.save((err)=>{
        res.status(200).json(user);
    });
}


exports.user_reserve = function(req,res){
    User.findById(req.body.userId,(err, user)=>{
        //console.log(user);
        user.reserve(req.body.bikeId, req.body.from, req.body.to, ()=>{
            //console.log('reserva !!!');
            res.status(200).send();
        });
    });
}