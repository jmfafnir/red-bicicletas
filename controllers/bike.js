var Bike = require('../Models/bike');

exports.bike_list = function(req, res) {
    
    Bike.allBikes((err,bikes)=>{
        res.render('bikes/index', {bikes: bikes});
    })
    
    
}

exports.bike_create_get = function(req, res){
    res.render('bikes/create')
}

exports.bike_create_post = function(req, res) {
    obj = {
        bike_id: req.body.bike_id,
        color: req.body.color, 
        bike_model:req.body.bike_model,
        location: [req.body.lat, req.body.lng]
    }    
    var bike = new Bike(obj);
    Bike.add(bike);

    res.redirect('/bikes');
}

exports.bike_delete_post = function(req, res){
    Bike.removeById(req.body.bike_id,()=>{
        res.redirect('/bikes');
    });    
}

exports.bike_update_get = function(req, res){
    
    Bike.findById(req.params.bike_id,(err,bike)=>{
        res.render('bikes/update',{bike});
    });
    
    
}

exports.bike_update_post = function(req, res) {
    
    obj = {
        bike_id: req.body.bike_id,
        color: req.body.color, 
        bike_model:req.body.bike_model,
        location: [req.body.lat, req.body.lng]
    };
    
    
    Bike.updateBike(req.params.bike_id,obj,(err,bike_update)=>{
        res.redirect('/bikes');
    });

    
      
}