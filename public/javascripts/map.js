var map = L.map('main_map').setView([5.0687821, -75.5186628], 13);


L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(map);


/**var marker = L.marker([5.068420916015233, -75.51317096463124]).addTo(map);
var marker = L.marker([5.067748401778625, -75.51699683603225]).addTo(map);
var marker = L.marker([5.06736410761483, -75.51114550330131]).addTo(map);*/

$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function(result){
        console.log(result);
        result.bikes.forEach(bike => {
            L.marker(bike.location, {title: bike.id}).addTo(map);
        });
    }
})