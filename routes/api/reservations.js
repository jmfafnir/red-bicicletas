var express = require('express');
var router = express.Router();
var reservationController = require('../../controllers/api/reservationControllerAPI');

router.get('/', reservationController.reservations_list);

module.exports = router;