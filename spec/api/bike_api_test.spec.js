var Bike = require('../../Models/bike');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

 
var base_url = "http://localhost:3000/api/bikes";

describe('Bike API',() => {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test_bikes';
        mongoose.connect(mongoDB,{ useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done)=>{
        Bike.deleteMany({}, function(err,success){
            if(err){
                console.log(err);
            }
            done();
        });
    });


    describe('GET BIKES /',()=>{
        it('Status 200', (done)=>{
            request.get(base_url,function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                //console.log(result);
                expect(result.bikes.length).toBe(0);
                done();
            })
        })
    });



    describe('POST BIKES /create',()=>{
        it('Status 200', (done)=>{
            var headers = {'content-type' : 'application/json'};
            var a = '{"bike_id":23,"color":"rojo","bike_model":"urbana","lat":23,"lng": 13}';
              
            request.post({
                headers : headers,
                url : base_url +'/create',
                body: a
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                
                var bike = JSON.parse(body).bike;
                //console.log(bike);
                expect(bike.bike_id).toBe(23);
                expect(bike.color).toBe("rojo");
                expect(bike.bike_model).toBe("urbana");
                done();
            })
        })
    })


    describe('DELETE BIKES /delete',()=>{
        it('Status 200', (done)=>{
            obj = {
                bike_id: 23,
                color: 'rojo', 
                bike_model:'urbana',
                location: [5.068420916015233, 75.51317096463124]
            } 
            Bike.add(obj,()=>{
                //console.log(obj);
                Bike.findById(obj.bike_id,(err,targetBike)=>{
                    expect(targetBike.bike_id).toEqual(23);
                    expect(targetBike.color).toEqual(obj.color);
                    expect(targetBike.bike_model).toBe(obj.bike_model);

                    var headers = {'content-type' : 'application/json'};
                    var id = '{"bike_id":23}';
              
                    request.delete({
                        headers : headers,
                        url : 'http://localhost:3000/api/bikes/delete',
                        body: id
                    },function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        Bike.allBikes(function(err, bikes){
                            expect(bikes.length).toEqual(0);        
                            done();
                        });
                        
                    });
                });

            });            
        });
    });

    describe('UPDATE BIKES /update',()=>{
        it('Status 200', (done)=>{
            obj = {
                bike_id: 23,
                color: 'rojo', 
                bike_model:'urbana',
                location: [5.068420916015233, 75.51317096463124]
            } 
            Bike.add(obj,()=>{
                Bike.findById(obj.bike_id,(err,targetBike)=>{
                    expect(targetBike.bike_id).toEqual(23);
                    expect(targetBike.color).toEqual(obj.color);
                    expect(targetBike.bike_model).toBe(obj.bike_model);

                    var headers = {'content-type' : 'application/json'};
                    var body = '{"bike_id":23,"color":"magenta","bike_model":"rural","lat":23,"lng": 13}';
                    var propertiesObject = { bike_id:'23'};
              
                    request.put({
                        headers : headers,
                        url : 'http://localhost:3000/api/bikes/update',
                        body: body,
                        qs: propertiesObject
                    },function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        var status = JSON.parse(body).status;
                        //console.log(status);
                        expect(status.modifiedCount).toBe(1);
                        Bike.findById(23, function(err, targetBike){
                            expect(targetBike.bike_id).toBe(23);
                            expect(targetBike.color).toBe("magenta");
                            expect(targetBike.bike_model).toBe("rural");
    
                            done();
                        });
                        
                    });
                });

            });            
        });
    });
})

