var Bike = require('../../Models/bike');
var User = require('../../Models/user');
var Reservation = require('../../Models/reservation');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

 
var base_url = "http://localhost:3000/api/reservations";

describe('User API',() => {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test_bikes';
        mongoose.connect(mongoDB,{ useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done)=>{
        Reservation.deleteMany({}, (err,success)=>{
            if(err) console.log(err);
            User.deleteMany({},(err,success)=>{
                if(err) console.log(err);
                Bike.deleteMany({}, function(err,success){
                    if(err){
                        console.log(err);
                    }
                    
                    done();
                });
            });
        });
    });

    describe('GET RESERVATIONS /',()=>{
        it('Status 200', (done)=>{
            request.get(base_url,function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                console.log(result);
                expect(result.reservations.length).toBe(0);
                done();
            })
        })
    });

});