var Bike = require('../../Models/bike');
var User = require('../../Models/user');
var Reservation = require('../../Models/reservation');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

 
var base_url = "http://localhost:3000/api/users";

describe('User API',() => {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test_bikes';
        mongoose.connect(mongoDB,{ useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done)=>{
        Reservation.deleteMany({}, (err,success)=>{
            if(err) console.log(err);
            User.deleteMany({},(err,success)=>{
                if(err) console.log(err);
                Bike.deleteMany({}, function(err,success){
                    if(err){
                        console.log(err);
                    }
                    
                    done();
                });
            });
        });
    });

    describe('GET User /',()=>{
        it('Status 200', (done)=>{
            request.get(base_url,function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                //console.log(result);
                expect(result.users.length).toBe(0);
                done();
            })
        })
    });

    describe('POST USER /create',()=>{
        it('Status 200', (done)=>{
            
            var headers = {'content-type' : 'application/json'};
            var a = '{"name":"Ramon"}';
              
            request.post({
                headers : headers,
                url : base_url +'/create',
                body: a
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                
                var user = JSON.parse(body);
                //console.log(user);
                expect(user.name).toBe("Ramon");
                done();
            })
        })
    });

    describe('POST USER /reserve',()=>{
        it('Status 200', (done)=>{
            const user = new User({name: 'Jose'});
            user.save();
            const bike = new Bike({bike_id: 1, color: "Azul", bike_model: "Montaña"});
            bike.save();
            
            var headers = {'content-type' : 'application/json'};
            var a = `{"userId":"${user.id}","bikeId":"${bike.id}","from":"2022-03-27","to":"2022-03-28"}`;

            //console.log(a);
              
            request.post({
                headers : headers,
                url : base_url +'/reserve',
                body: a
            },function(error, response, body){
                expect(response.statusCode).toBe(200);
                Reservation.find({}).populate('bike').populate('user').exec((err,reservations)=>{
                    //console.log("---------------");
                    //console.log(reservations[0]);
                    expect(reservations.length).toBe(1);
                    expect(reservations[0].reservationDays()).toBe(2);
                    expect(reservations[0].bike.bike_id).toBe(1);
                    expect(reservations[0].user.name).toBe(user.name);
                    done();
                });
                
            })
        })
    })

});