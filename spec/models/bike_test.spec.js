var mongoose = require('mongoose');
var Bike = require('../../Models/bike');


describe('Test Bike Model',function(){
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test_bikes';
        mongoose.connect(mongoDB,{ useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done)=>{
        Bike.deleteMany({}, function(err,success){
            if(err){
                console.log(err);
            }
            done();
        });
    });


    describe('Bike.createInstance', ()=>{
        it('create an instance of bike', () =>{
            var bike = Bike.createInstance(1,"verde","urbana", [-34.5, -54.1]);

            expect(bike.bike_id).toBe(1);
            expect(bike.color).toBe("verde");
            expect(bike.bike_model).toBe("urbana");
            expect(bike.location[0]).toEqual(-34.5);
            expect(bike.location[1]).toEqual(-54.1);
        });        
    });

    describe('Bike.allBikes',()=> {
        it('Start void',(done)=>{
            Bike.allBikes(function(err,bikes){
                expect(bikes.length).toBe(0);
                done();
            });
        });
    });

    describe('Bike.add', () =>{
        it('Add one bike', (done) =>{
            var aBike = new Bike({bike_id: 1, color: "verde", bike_model:"urbana"});
            Bike.add(aBike, function(err, newBike){
                if(err) console.log(err);
                Bike.allBikes(function(err, bikes){
                    expect(bikes.length).toEqual(1);
                    expect(bikes[0].bike_id).toEqual(aBike.bike_id);

                    done();
                });
            });
        });
    });
    
    describe('Bike.findById', () =>{
        it('must return the bike with id 1', (done) =>{
            var aBike = new Bike({bike_id: 1, color: "verde", bike_model:"urbana"});
            Bike.add(aBike, function(err, newBike){
                if(err) console.log(err);
                var aBike2 = new Bike({bike_id: 2, color: "roja", bike_model:"rural"});
                Bike.add(aBike2, function(err, newBike){
                    if(err) console.log(err);
                    Bike.findById(1, function(err, targetBike){
                        expect(targetBike.bike_id).toBe(aBike.bike_id);
                        expect(targetBike.color).toBe(aBike.color);
                        expect(targetBike.bike_model).toBe(aBike.bike_model);

                        done();
                    });    
                });                
            });
        });
    });

    describe('Bike.removeById', () =>{
        it('the length of the list bikes must by 1, and the bike returned have de id 2', (done) =>{
            var aBike = new Bike({bike_id: 1, color: "verde", bike_model:"urbana"});
            Bike.add(aBike, function(err, newBike){
                if(err) console.log(err);
                var aBike2 = new Bike({bike_id: 2, color: "roja", bike_model:"rural"});
                Bike.add(aBike2, function(err, newBike){
                    if(err) console.log(err);
                    Bike.removeById(2, function(err, target){
                        expect(target.deletedCount).toBe(1);
                        Bike.allBikes(function(err, bikes){
                            expect(bikes.length).toEqual(1);
        
                            done();
                        });                        
                    });    
                });                
            });
        });
    });

    describe('Bike.updateBike', () =>{
        it('the bike with id 2 changes its color to magenta and its bike model to mountain', (done) =>{
            var aBike = new Bike({bike_id: 1, color: "verde", bike_model:"urbana"});
            Bike.add(aBike, function(err, newBike){
                if(err) console.log(err);
                var aBike2 = new Bike({bike_id: 2, color: "roja", bike_model:"rural"});
                Bike.add(aBike2, function(err, newBike){
                    if(err) console.log(err);
                    obj = {
                        bike_id: 2,
                        color: 'magenta', 
                        bike_model:'urbana',
                        location: [5.068420916015233, 75.51317096463124]
                    }
                    Bike.updateBike(2,obj,function(err, targetBike){
                        expect(targetBike.modifiedCount).toBe(1);
                        Bike.findById(2, function(err, targetBike){
                            expect(targetBike.bike_id).toBe(obj.bike_id);
                            expect(targetBike.color).toBe(obj.color);
                            expect(targetBike.bike_model).toBe(obj.bike_model);
    
                            done();
                        });
                                             
                    });  
                });                
            });
        });
    });
    

});

/* beforeEach(()=>{
    Bike.allBikes = []
})

describe('Bike.allBikes',()=>{
   it('Start void', ()=>{
       expect(Bike.allBikes.length).toBe(0);
   }) 
})

describe('Bike.add',()=>{
    it('Add one', ()=>{
        expect(Bike.allBikes.length).toBe(0);
        var a = new Bike(1, 'rojo', 'urbana', [5.068420916015233, -75.51317096463124]);
        Bike.add(a);
        expect(Bike.allBikes.length).toBe(1);
        expect(Bike.allBikes[0]).toBe(a);
        
    }) 
 })

 describe('Bike.findById',()=>{
    it('he must return the bike with id 1', ()=>{
        expect(Bike.allBikes.length).toBe(0);
        
        var a = new Bike(1, 'rojo', 'urbana', [5.068420916015233, -75.51317096463124]);
        var b = new Bike(2, 'blanca', 'urbana', [5.067748401778625, -75.51699683603225]);
        Bike.add(a);
        Bike.add(b);

        var targetBike = Bike.findById(1);
        expect(targetBike.id).toEqual(1);
        expect(targetBike.color).toBe(a.color);
        expect(targetBike.bike_model).toBe(a.bike_model);
        
    }) 
 })

 describe('Bike.removeById',()=>{
    it('remove bike with id 2 expect exception', ()=>{
        expect(Bike.allBikes.length).toBe(0);
        
        var a = new Bike(1, 'rojo', 'urbana', [5.068420916015233, -75.51317096463124]);
        var b = new Bike(2, 'blanca', 'urbana', [5.067748401778625, -75.51699683603225]);
        Bike.add(a);
        Bike.add(b);

        var targetBike = Bike.removeById(2);
        expect(()=>{ Bike.findById(2);}).toThrowError(`The bicycle with the id ${b.id} does not exist`);
        
    }) 
 }) */