var mongoose = require('mongoose');
var Bike = require('../../Models/bike');
var User = require('../../Models/user');
var Reservation = require('../../Models/reservation');


describe('Testing Reservation Model', ()=> {
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/test_bikes';
        mongoose.connect(mongoDB,{ useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });


    afterEach((done)=>{
        Reservation.deleteMany({}, (err,success)=>{
            if(err) console.log(err);
            User.deleteMany({},(err,success)=>{
                if(err) console.log(err);
                Bike.deleteMany({}, function(err,success){
                    if(err){
                        console.log(err);
                    }
                    
                    done();
                });
            });
        });
    });

    describe('the reservation calculate reservation days ', ()=>{
        it('the reservation exist and the days of reservation is 8', (done)=> {
            const user = new User({name: 'Jose'});
            user.save();
            const bike = new Bike({bike_id: 1, color: "Azul", bike_model: "Montaña"});
            bike.save();
            
            var today = new Date();
            var aWeekAfter = new Date();

            aWeekAfter.setDate(today.getDate()+7);

            user.reserve(bike.id,today,aWeekAfter, (err, reservation)=>{
                Reservation.find({}).populate('bike').populate('user').exec((err,reservations)=>{
                    console.log("---------------");
                    console.log(reservations[0]);
                    expect(reservations.length).toBe(1);
                    expect(reservations[0].reservationDays()).toBe(8);
                    expect(reservations[0].bike.bike_id).toBe(1);
                    expect(reservations[0].user.name).toBe(user.name);
                    done();
                });
            });

        });
    });


});